package main

import (
	"fmt"
	"github.com/imroc/req"
	"log"
	"net"
	"strings"
)

const IP_RANGES_URL = "https://ip-ranges.amazonaws.com/ip-ranges.json"

type Prefix struct {
	IpPrefix   string `json:"ip_prefix"`
	IpPrefixV6 string `json:"ipv6_prefix"`
	Region     string `json:"region"`
	Service    string `json:"service"`
}

func (p *Prefix) GetIpPrefix() string {
	if p.IpPrefix != "" {
		return p.IpPrefix
	} else {
		return p.IpPrefixV6
	}
}

// AWSIPRange is read from https://ip-ranges.amazonaws.com/ip-ranges.json
type AWSIPRange struct {
	CreateDate   string   `json:"createDate"`
	SyncToken    string   `json:"syncToken"`
	IpV4Prefixes []Prefix `json:"prefixes"`
	IpV6Prefixes []Prefix `json:"ipv6_prefixes"`
}

func getIpRanges() *AWSIPRange {
	headers := req.Header{
		"Accept":     "application/json",
		"User-Agent": "aws-lambda-ipcalc",
	}
	r, _ := req.Get(IP_RANGES_URL, headers)

	var ipRange AWSIPRange
	r.ToJSON(&ipRange)

	return &ipRange
}

func findIpInRanges(ipAddress string, ipRange *AWSIPRange) string {

	var prefixes []Prefix
	if strings.Contains(ipAddress, ":") {
		prefixes = ipRange.IpV6Prefixes
	} else {
		prefixes = ipRange.IpV4Prefixes
	}

	contains := false
	var foundPrefixes []Prefix
	for _, p := range prefixes {
		ip := net.ParseIP(ipAddress)
		_, cidrnet, _ := net.ParseCIDR(p.GetIpPrefix())

		if cidrnet.Contains(ip) {
			contains = true
			foundPrefixes = append(foundPrefixes, p)
		}
	}

	if contains {
		log.Printf("IP %s found in AWS ip range\n", ipAddress)
		log.Printf("Services: %s\n", getAllServices(foundPrefixes))
		log.Printf("Regions: %s\n", getAllRegions(foundPrefixes))
		return fmt.Sprintf("IP %s found in AWS ip range", ipAddress)
	} else {
		return fmt.Sprintf("IP %s not found in AWS ip range", ipAddress)
	}
}

func getAllServices(prefixes []Prefix) []string {
	var services []string
	encountered := map[string]bool{}

	for _, p := range prefixes {
		if encountered[p.Region] == true {

		} else {
			encountered[p.Service] = true
			services = append(services, p.Service)
		}
	}
	return services
}

func getAllRegions(prefixes []Prefix) []string {
	var regions []string
	encountered := map[string]bool{}

	for _, p := range prefixes {
		if encountered[p.Region] == false {
			encountered[p.Region] = true
			regions = append(regions, p.Region)
		}
	}
	return regions
}
