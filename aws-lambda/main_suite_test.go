package main

import (
	"github.com/aws/aws-lambda-go/events"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
	"testing"
)

func TestWhoisCloudLambda(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Whois.cloud Lambda")
}

var _ = Describe("Whois.cloud Lambda", func() {

	Describe("where source ip is IPv4", func() {
		Context("and is not AWS", func() {
			It("should return that IP is not AWS", func() {
				req := events.APIGatewayProxyRequest{
					RequestContext: events.APIGatewayProxyRequestContext{
						Identity: events.APIGatewayRequestIdentity{
							SourceIP: "1.1.1.1",
						},
					},
				}

				response, err := Handler(req)

				Expect(err).NotTo(HaveOccurred())
				Expect(response.Body).Should(Equal("IP 1.1.1.1 not found in AWS ip range"))
				Expect(response.StatusCode).Should(Equal(http.StatusOK))
			})
		})

		Context("and is AWS", func() {
			It("should return that IP is AWS", func() {
				req := events.APIGatewayProxyRequest{
					RequestContext: events.APIGatewayProxyRequestContext{
						Identity: events.APIGatewayRequestIdentity{
							SourceIP: "54.155.0.1",
						},
					},
				}

				response, err := Handler(req)

				Expect(err).NotTo(HaveOccurred())
				Expect(response.Body).Should(Equal("IP 54.155.0.1 found in AWS ip range"))
				Expect(response.StatusCode).Should(Equal(http.StatusOK))
			})
		})
	})

	Describe("where source ip is IPv6", func() {
		Context("and is not AWS", func() {
			It("should return that IP is not AWS", func() {
				req := events.APIGatewayProxyRequest{
					RequestContext: events.APIGatewayProxyRequestContext{
						Identity: events.APIGatewayRequestIdentity{
							SourceIP: "2606:4700:4700::1111",
						},
					},
				}

				response, err := Handler(req)

				Expect(err).NotTo(HaveOccurred())
				Expect(response.Body).Should(Equal("IP 2606:4700:4700::1111 not found in AWS ip range"))
				Expect(response.StatusCode).Should(Equal(http.StatusOK))
			})
		})

		Context("and is AWS", func() {
			It("should return that IP is AWS", func() {
				req := events.APIGatewayProxyRequest{
					RequestContext: events.APIGatewayProxyRequestContext{
						Identity: events.APIGatewayRequestIdentity{
							SourceIP: "2600:1ff8:4000::1",
						},
					},
				}

				response, err := Handler(req)

				Expect(err).NotTo(HaveOccurred())
				Expect(response.Body).Should(Equal("IP 2600:1ff8:4000::1 found in AWS ip range"))
				Expect(response.StatusCode).Should(Equal(http.StatusOK))
			})
		})
	})

})
