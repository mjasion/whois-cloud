package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"net/http"
)

var (
	// ErrNameNotProvided is thrown when a name is not provided
	ErrNameNotProvided = errors.New("no name was provided in the HTTP body")
)

func main() {
	lambda.Start(Handler)
}

func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ipRange := getIpRanges()
	return events.APIGatewayProxyResponse{
		Body:       findIpInRanges(request.RequestContext.Identity.SourceIP, ipRange),
		StatusCode: http.StatusOK,
	}, nil

}
